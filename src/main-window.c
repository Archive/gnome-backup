/* GNOME Backup Tool
 * Copyright (C) 2003 GNOME Foundation
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "common.h"
#include <gtk/gtktable.h>
#include <bonobo/bonobo-control.h>
#include <bonobo/bonobo-ui-component.h>
#include <bonobo/bonobo-window.h>
#include <libgnome/gnome-i18n.h>
#include <libgnomebackup/gnome-backup-destination-selector.h>
#include <libgnomebackup/gnome-backup-fileset-list.h>

static GtkWidget *main_window, *fileset_list;

static void
command_close (BonoboUIComponent *uic, void *data, const char *path)
{
	gtk_widget_destroy (main_window);
}

static gboolean
window_delete_event_cb (GtkWidget *widget, GdkEventAny *event, gpointer user_data)
{
	gtk_widget_destroy (main_window);
}

static void
window_destroyed_cb (GtkObject *object, gpointer user_data)
{
	bonobo_main_quit ();
}

static BonoboUIVerb command_verbs [] = {
	BONOBO_UI_VERB ("FileClose", command_close),
};

void
main_window_init (void)
{
	GtkWidget *table, *combo;
	BonoboUIComponent *uic;
	BonoboUIContainer *container;
	Bonobo_UIContainer corba_container;
	BonoboControl *control;

	/* create the window */
	main_window = bonobo_window_new (_("gnome-backup"), _("GNOME Backup Tool"));
	g_signal_connect (G_OBJECT (main_window), "delete_event", G_CALLBACK (window_delete_event_cb), NULL);
	g_signal_connect (G_OBJECT (main_window), "destroy", G_CALLBACK (window_destroyed_cb), NULL);

	table = gtk_table_new (3, 2, FALSE);
	gtk_widget_show (table);
	bonobo_window_set_contents (BONOBO_WINDOW (main_window), table);

	/* set the menubar/toolbar */
	container = bonobo_window_get_ui_container (BONOBO_WINDOW (main_window));
        corba_container = BONOBO_OBJREF (container);
        uic = bonobo_ui_component_new ("gnome-backup");
        bonobo_ui_component_set_container (uic, corba_container, NULL);

	bonobo_ui_component_freeze (uic, NULL);
	bonobo_ui_util_set_ui (uic, GNOME_BACKUP_UIDIR, "gnome-backup.xml", "gnome-backup", NULL);
	bonobo_ui_component_add_verb_list_with_data (uic, command_verbs, NULL);
	bonobo_ui_component_thaw (uic, NULL);

	/* create the destination combo box */
	combo = gnome_backup_destination_selector_new ();
	gtk_widget_show (combo);

	control = bonobo_control_new (combo);
	bonobo_ui_component_object_set (uic, "/Toolbar/BackupToCombo", BONOBO_OBJREF (control), NULL);
	bonobo_object_unref (control);
	
	/* create the fileset list */
	fileset_list = gnome_backup_fileset_list_new ();
	gtk_widget_show (fileset_list);
	gtk_table_attach (GTK_TABLE (table), fileset_list, 0, 1, 1, 2,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK, 6, 6);

	gtk_widget_show (main_window);
}
