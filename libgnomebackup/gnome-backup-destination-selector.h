/* GNOME Backup library
 * Copyright (C) 2003 GNOME Foundation
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gnome_backup_destination_combo_h__)
#  define __gnome_backup_destination_combo_h__

#include <gtk/gtkoptionmenu.h>

G_BEGIN_DECLS

#define GNOME_BACKUP_TYPE_DESTINATION_SELECTOR            (gnome_backup_destination_selector_get_type())
#define GNOME_BACKUP_DESTINATION_SELECTOR(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_BACKUP_TYPE_DESTINATION_SELECTOR, GnomeBackupDestinationSelector))
#define GNOME_BACKUP_DESTINATION_SELECTOR_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_BACKUP_TYPE_DESTINATION_SELECTOR, GnomeBackupDestinationSelectorClass))
#define GNOME_BACKUP_IS_DESTINATION_SELECTOR(obj)         (G_TYPE_CHECK_INSTANCE_TYPE(obj, GNOME_BACKUP_TYPE_DESTINATION_SELECTOR))
#define GNOME_BACKUP_IS_DESTINATION_SELECTOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GNOME_BACKUP_TYPE_DESTINATION_SELECTOR))

typedef struct _GnomeBackupDestinationSelectorPrivate GnomeBackupDestinationSelectorPrivate;

typedef struct {
	GtkOptionMenu parent;
	GnomeBackupDestinationSelectorPrivate *priv;
} GnomeBackupDestinationSelector;

typedef struct {
	GtkOptionMenuClass parent_class;
} GnomeBackupDestinationSelectorClass;

GType      gnome_backup_destination_selector_get_type (void);
GtkWidget *gnome_backup_destination_selector_new (void);

G_END_DECLS

#endif
