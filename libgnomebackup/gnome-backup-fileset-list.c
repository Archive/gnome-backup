/* GNOME Backup library
 * Copyright (C) 2003 GNOME Foundation
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtkcellrenderertext.h>
#include <gtk/gtkliststore.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtktreeview.h>
#include <libgnome/gnome-i18n.h>
#include <libgnomebackup/gnome-backup-fileset-list.h>

struct _GnomeBackupFilesetListPrivate {
	GtkWidget *list;
	GtkListStore *model;
};

static void gbfl_class_init (GnomeBackupFilesetListClass *klass);
static void gbfl_init (GnomeBackupFilesetList *fl, GnomeBackupFilesetListClass *klass);
static void gbfl_finalize (GObject *object);

static GObjectClass *parent_class = NULL;

static void
refresh_file_list (GnomeBackupFilesetList *fl)
{
	GSList *list;

	/* clear previous contents */
	gtk_list_store_clear (GTK_LIST_STORE (fl->priv->model));

	for (list = gnome_backup_fileset_get_list (); list; list = list->next) {
		GtkTreeIter iter;
		const GDate *last_backup;
		gchar tstr[256];
		GnomeBackupFileset *fs = list->data;

		if (!fs)
			continue;

		last_backup = gnome_backup_fileset_get_last_backup (fs);
		if (!last_backup)
			strcpy (tstr, "");
		else
			g_date_strftime (tstr, sizeof (tstr), _("%B %d %Y"), last_backup);

		gtk_list_store_append (GTK_LIST_STORE (fl->priv->model), &iter);
		gtk_list_store_set (GTK_LIST_STORE (fl->priv->model), &iter,
				    0, gnome_backup_fileset_get_name (fs),
				    1, "--",
				    2, tstr,
				    -1);
	}

	g_slist_free (list);
}

static void
treeview_row_activated_cb (GtkTreeView *tree_view, GtkTreePath *path,
			   GtkTreeViewColumn *column, gpointer user_data)
{
	GtkTreeIter iter;
	gchar *fs_name = NULL;
	const GnomeBackupFileset *fs;
	GnomeBackupFilesetList *fl = user_data;

	if (!gtk_tree_model_get_iter (GTK_TREE_MODEL (fl->priv->model), &iter, path))
		return;

	gtk_tree_model_get (GTK_TREE_MODEL (fl->priv->model), &iter, 0, &fs_name, -1);
	if (!fs_name)
		return;

	fs = gnome_backup_fileset_get_by_name (fs_name);
	if (!fs)
		return;

	/* FIXME */
}

static void
gbfl_class_init (GnomeBackupFilesetListClass *klass)
{
	GObjectClass *object_class;

	parent_class = g_type_class_peek_parent (klass);

	object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = gbfl_finalize;
}

static void
gbfl_init (GnomeBackupFilesetList *fl, GnomeBackupFilesetListClass *klass)
{
	GtkWidget *sw;
	GtkCellRenderer *cell_renderer;
	GtkTreeViewColumn *column;

	fl->priv = g_new0 (GnomeBackupFilesetListPrivate, 1);

	/* create the model */
	fl->priv->model = gtk_list_store_new (3, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);

	/* create the widgets */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_container_set_border_width (GTK_CONTAINER (sw), 6);
	gtk_widget_show (sw);
	gtk_box_pack_start (GTK_BOX (fl), sw, TRUE, TRUE, 6);

	fl->priv->list = gtk_tree_view_new_with_model ((GtkTreeModel *) fl->priv->model);
	gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (fl->priv->list), TRUE);

	cell_renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Items"), cell_renderer, "text", 0, NULL);
	gtk_tree_view_column_set_resizable (column, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (fl->priv->list), column);

	cell_renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Size"), cell_renderer, "text", 1, NULL);
	gtk_tree_view_column_set_resizable (column, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (fl->priv->list), column);

	cell_renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Last backed up"), cell_renderer, "text", 2, NULL);
	gtk_tree_view_column_set_resizable (column, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (fl->priv->list), column);

	/* set up the selection signals */
	g_signal_connect (fl->priv->list, "row_activated", G_CALLBACK (treeview_row_activated_cb), fl);

	/* add tree view to scrolled window */
	gtk_widget_show (fl->priv->list);
	gtk_container_add (GTK_CONTAINER (sw), fl->priv->list);
}

static void
gbfl_finalize (GObject *object)
{
	GnomeBackupFilesetList *fl = (GnomeBackupFilesetList *) object;

	if (fl->priv) {
		if (fl->priv->model) {
			g_object_unref (fl->priv->model);
			fl->priv->model = NULL;
		}

		g_free (fl->priv);
		fl->priv = NULL;
	}

	if (parent_class->finalize)
		parent_class->finalize (object);
}

GType
gnome_backup_fileset_list_get_type (void)
{
	static GType type = 0;
                                                                                                  
        if (type == 0) {
                static GTypeInfo info = {
                        sizeof (GnomeBackupFilesetListClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) gbfl_class_init,
                        NULL, NULL,
                        sizeof (GnomeBackupFilesetList),
                        0,
                        (GInstanceInitFunc) gbfl_init
                };
		type = g_type_register_static (GTK_TYPE_VBOX, "GnomeBackupFilesetList", &info, 0);
        }

        return type;
}

GtkWidget *
gnome_backup_fileset_list_new (void)
{
	GnomeBackupFilesetList *fl;

	fl = g_object_new (gnome_backup_fileset_list_get_type (), NULL);
	refresh_file_list (fl);

	return GTK_WIDGET (fl);
}

const GnomeBackupFileset *
gnome_backup_fileset_list_get_current (GnomeBackupFilesetList *fl)
{
	GtkTreeIter iter;
	gchar *fs_name = NULL;
	const GnomeBackupFileset *fs;

	g_return_val_if_fail (GNOME_BACKUP_IS_FILESET_LIST (fl), NULL);

	if (!gtk_tree_selection_get_selected (gtk_tree_view_get_selection (GTK_TREE_VIEW (fl->priv->list)),
					      NULL, &iter))
		return NULL;

	gtk_tree_model_get (GTK_TREE_MODEL (fl->priv->model), &iter, 0, &fs_name, -1);
	if (!fs_name)
		return NULL;

	fs = gnome_backup_fileset_get_by_name (fs_name);
	if (!fs)
		return NULL;

	return (const GnomeBackupFileset *) fs;
}
