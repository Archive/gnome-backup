/* GNOME Backup library
 * Copyright (C) 2003 GNOME Foundation
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include "gnome-backup-destination-selector.h"

struct _GnomeBackupDestinationSelectorPrivate {
};

static void gbds_class_init (GnomeBackupDestinationSelectorClass *klass);
static void gbds_init (GnomeBackupDestinationSelector *ds, GnomeBackupDestinationSelectorClass *klass);
static void gbds_finalize (GObject *object);

static GObjectClass *parent_class = NULL;

static void
gbds_class_init (GnomeBackupDestinationSelectorClass *klass)
{
	GObjectClass *object_class;

	parent_class = g_type_class_peek_parent (klass);

	object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = gbds_finalize;
}

static void
gbds_init (GnomeBackupDestinationSelector *ds, GnomeBackupDestinationSelectorClass *klass)
{
	ds->priv = g_new0 (GnomeBackupDestinationSelectorPrivate, 1);
}

static void
gbds_finalize (GObject *object)
{
	GnomeBackupDestinationSelector *ds = (GnomeBackupDestinationSelector *) object;

	if (ds->priv) {
		g_free (ds->priv);
		ds->priv = NULL;
	}

	parent_class->finalize (object);
}

GType
gnome_backup_destination_selector_get_type (void)
{
	static GType type = 0;
                                                                                                  
        if (type == 0) {
                static GTypeInfo info = {
                        sizeof (GnomeBackupDestinationSelectorClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) gbds_class_init,
                        NULL, NULL,
                        sizeof (GnomeBackupDestinationSelector),
                        0,
                        (GInstanceInitFunc) gbds_init
                };
		type = g_type_register_static (GTK_TYPE_OPTION_MENU, "GnomeBackupDestinationSelector", &info, 0);
        }

        return type;
}

/**
 * gnome_backup_destination_selector_new
 */
GtkWidget *
gnome_backup_destination_selector_new (void)
{
	return g_object_new (GNOME_BACKUP_TYPE_DESTINATION_SELECTOR, NULL);
}
