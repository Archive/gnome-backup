/* GNOME Backup library
 * Copyright (C) 2003 GNOME Foundation
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <libgda/gda-xml-database.h>
#include <gconf/gconf-client.h>
#include <libgnome/gnome-i18n.h>
#include <libgnomebackup/gnome-backup-fileset.h>

struct _GnomeBackupFilesetPrivate {
	gchar *name;
	GHashTable *paths;
	GDate *last_backup;
};

static void gbf_class_init (GnomeBackupFilesetClass *klass);
static void gbf_init (GnomeBackupFileset *fileset, GnomeBackupFilesetClass *klass);
static void gbf_finalize (GObject *object);

static GObjectClass *parent_class = NULL;
static GdaXmlDatabase *conf_file = NULL;
static GHashTable *loaded_filesets = NULL;

static void
gbf_class_init (GnomeBackupFilesetClass *klass)
{
	GObjectClass *object_class;

	parent_class = g_type_class_peek_parent (klass);

	object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = gbf_finalize;
}

static void
gbf_init (GnomeBackupFileset *fs, GnomeBackupFilesetClass *klass)
{
	fs->priv = g_new0 (GnomeBackupFilesetPrivate, 1);

	fs->priv->paths = g_hash_table_new (g_str_hash, g_str_equal);
}

static void
remove_path (gpointer key, gpointer value, gpointer user_data)
{
	g_free (key);
	g_free (value);
}

static void
gbf_finalize (GObject *object)
{
	GnomeBackupFileset *fs = (GnomeBackupFileset *) object;

	g_return_if_fail (GNOME_BACKUP_IS_FILESET (fs));

	/* free memory */
	if (fs->priv) {
		if (fs->priv->name) {
			g_free (fs->priv->name);
			fs->priv->name = NULL;
		}

		if (fs->priv->paths) {
			g_hash_table_foreach (fs->priv->paths, (GHFunc) remove_path, NULL);
			g_hash_table_destroy (fs->priv->paths);
			fs->priv->paths = NULL;
		}
 
		if (fs->priv->last_backup) {
			g_date_free (fs->priv->last_backup);
			fs->priv->last_backup = NULL;
		}

		g_free (fs->priv);
		fs->priv = NULL;
	}

	if (parent_class->finalize)
		parent_class->finalize (object);
}

GType
gnome_backup_fileset_get_type (void)
{
   static GType type = 0;
                                                                                
        if (type == 0) {
                static GTypeInfo info = {
                        sizeof (GnomeBackupFilesetClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) gbf_class_init,
                        NULL, NULL,
                        sizeof (GnomeBackupFileset),
                        0,
                        (GInstanceInitFunc) gbf_init
                };
        type = g_type_register_static (G_TYPE_OBJECT, "GnomeBackupFileset", &info, 0);
        }
        return type;
}

/**
 * gnome_backup_fileset_new
 *
 * Create a new empty #GnomeBackupFilset object, which contains information
 * about a set of files to be backed up.
 *
 * Returns: the newly created object.
 */
GnomeBackupFileset *
gnome_backup_fileset_new (void)
{
	return g_object_new (GNOME_BACKUP_TYPE_FILESET, NULL);
}

/**
 * gnome_backup_fileset_get_name
 * @fs: A #GnomeBackupFileset object.
 *
 * Retrieve the name used for identifying the given fileset object.
 *
 * Returns: the name of the fileset.
 */
const gchar *
gnome_backup_fileset_get_name (GnomeBackupFileset *fs)
{
	g_return_val_if_fail (GNOME_BACKUP_IS_FILESET (fs), NULL);
	return (const gchar *) fs->priv->name;
}

/**
 * gnome_backup_fileset_set_name
 */
void
gnome_backup_fileset_set_name (GnomeBackupFileset *fs, const gchar *name)
{
	g_return_if_fail (GNOME_BACKUP_IS_FILESET (fs));
	g_return_if_fail (name != NULL);

	/* FIXME: check there is no other filesets with that name */

	if (fs->priv->name)
		g_free (fs->priv->name);
	fs->priv->name = g_strdup (name);
}

/**
 * gnome_backup_fileset_get_last_backup
 */
const GDate *
gnome_backup_fileset_get_last_backup (GnomeBackupFileset *fs)
{
	g_return_val_if_fail (GNOME_BACKUP_IS_FILESET (fs), NULL);
	return (const GDate *) fs->priv->last_backup;
}

/**
 * gnome_backup_fileset_set_last_backup
 */
void
gnome_backup_fileset_set_last_backup (GnomeBackupFileset *fs, const GDate *last_backup)
{
	g_return_if_fail (GNOME_BACKUP_IS_FILESET (fs));

	if (fs->priv->last_backup) {
		g_date_free (fs->priv->last_backup);
		fs->priv->last_backup = NULL;
	}

	if (last_backup) {
		fs->priv->last_backup = g_date_new_dmy (g_date_get_day (last_backup),
							g_date_get_month (last_backup),
							g_date_get_year (last_backup));
	}
}

/**
 * gnome_backup_fileset_add_path
 */
void
gnome_backup_fileset_add_path (GnomeBackupFileset *fs, const gchar *path)
{
	g_return_if_fail (GNOME_BACKUP_IS_FILESET (fs));
	g_return_if_fail (path != NULL);

	/* refuse to add an already existant path */
	if (!g_hash_table_lookup (fs->priv->paths, path)) {
		g_hash_table_insert (fs->priv->paths, g_strdup (path), g_strdup (path));
	}
}

static void
add_fileset (const gchar *name,
	     const gchar *paths[],
	     GDate *last_backup)
{
	GnomeBackupFileset *fs;
	gint i;

	fs = gnome_backup_fileset_new ();
	gnome_backup_fileset_set_name (fs, name);
	gnome_backup_fileset_set_last_backup (fs, last_backup);

	for (i = 0; i < G_N_ELEMENTS (paths); i++)
		gnome_backup_fileset_add_path (fs, paths[i]);

	g_hash_table_insert (loaded_filesets, g_strdup (name), fs);
}

static void
add_field_to_table (GdaTable *table, const gchar *name, GdaValueType type, gint size, gboolean pk)
{
	GdaFieldAttributes *fa;

	fa = gda_field_attributes_new ();
	gda_field_attributes_set_name (fa, name);
	gda_field_attributes_set_gdatype (fa, type);
	gda_field_attributes_set_defined_size (fa, size);
	gda_field_attributes_set_primary_key (fa, pk);

	gda_table_add_field (table, (const GdaFieldAttributes *) fa);

	gda_field_attributes_free (fa);
}

static void
create_default_tables (void)
{
	GdaTable *table;

	/* create the 'filesets' table */
	table = gda_xml_database_new_table (conf_file, "filesets");
	add_field_to_table (table, "id", GDA_VALUE_TYPE_STRING, 80, TRUE);
	add_field_to_table (table, "paths", GDA_VALUE_TYPE_STRING, -1, FALSE);
	add_field_to_table (table, "last_backup", GDA_VALUE_TYPE_DATE, -1, FALSE);
}

static void
ensure_loaded (void)
{
	if (!loaded_filesets)
		loaded_filesets = g_hash_table_new (g_str_hash, g_str_equal);

	if (!conf_file) {
		gchar *uri;
		const gchar *paths[1];
		GdaTable *table;

		/* create the directory for the data files */
		uri = g_build_path ("/", g_get_home_dir (), ".gnome2", "gnome-backup", NULL);
		if (g_file_test (uri, G_FILE_TEST_IS_DIR)) {
			g_free (uri);

			/* load the file */
			uri = g_build_path ("/", g_get_home_dir (), ".gnome2", "gnome-backup", "filesets.xml", NULL);
			conf_file = gda_xml_database_new_from_uri (uri);
			if (conf_file) {
				table = gda_xml_database_find_table (conf_file, "filesets");
				if (table) {
				} else {
					create_default_tables ();
					gda_xml_database_save (conf_file, uri);
				}
			}

			g_free (uri);
		} else {
			mkdir (uri, S_IRUSR | S_IWUSR | S_IXUSR);
			g_free (uri);

			/* create the database structure */
			uri = g_build_path ("/", g_get_home_dir (), ".gnome2", "gnome-backup", "filesets.xml", NULL);
			conf_file = gda_xml_database_new ();
			gda_xml_database_set_uri (conf_file, uri);
			create_default_tables ();

			/* add home directory */
			paths[0] = g_get_home_dir ();
			add_fileset (_("Home directory"), paths, NULL);

			/* add Evolution data */
			paths[0] = g_build_path ("/", g_get_home_dir (), "evolution", NULL);
			if (g_file_test (paths[0], G_FILE_TEST_IS_DIR))
				add_fileset (_("Evolution data"), paths, NULL);
			g_free ((gpointer) paths[0]);

			gda_xml_database_save (conf_file, uri);

			g_free (uri);
		}
	}
}

/**
 * gnome_backup_fileset_get_by_name
 * @name: Name of the fileset to get.
 *
 * Search in the configuration for a fileset identified by the given name.
 *
 * Returns: a #GnomeBackupFileset object indetifying the fileset retrieved
 * from the configuration, or NULL if not found. If the return value is not
 * NULL, it points to internal data of the #GnomeBackupFileset, so don't
 * ever free it.
 */
const GnomeBackupFileset *
gnome_backup_fileset_get_by_name (const gchar *name)
{
	GdaTable *table;
	gint rows, cols, r, c;

	g_return_val_if_fail (name != NULL, NULL);

	ensure_loaded ();

	return (const GnomeBackupFileset *) g_hash_table_lookup (loaded_filesets, name);
}

static void
add_fileset_to_slist (gpointer key, gpointer value, gpointer user_data)
{
	GSList **list = (GSList **) user_data;

	*list = g_slist_prepend (*list, value);
}

/**
 * gnome_backup_fileset_get_list
 *
 * Get a list of all configured filesets,
 *
 * Returns: a GSList containing all the configured filesets, When no longer
 * needed, the list should be freed by calling g_slist_free.
 */
GSList *
gnome_backup_fileset_get_list (void)
{
	GSList *list = NULL;

	ensure_loaded ();

	g_hash_table_foreach (loaded_filesets, (GHFunc) add_fileset_to_slist, &list);
	return list;
}
