/* GNOME Backup library
 * Copyright (C) 2003 GNOME Foundation
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gnome_backup_destination_h__)
#  define __gnome_backup_destination_h__

#include <glib-object.h>
 
G_BEGIN_DECLS
 
#define GNOME_BACKUP_TYPE_DESTINATION            (gnome_backup_destination_get_type())
#define GNOME_BACKUP_DESTINATION(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_BACKUP_TYPE_DESTINATION, GnomeBackupDestination))
#define GNOME_BACKUP_DESTINATION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_BACKUP_TYPE_DESTINATION, GnomeBackupDestinationClass))
#define GNOME_BACKUP_IS_DESTINATION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE(obj, GNOME_BACKUP_TYPE_DESTINATION))
#define GNOME_BACKUP_IS_DESTINATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GNOME_BACKUP_TYPE_DESTINATION))

typedef struct _GnomeBackupDestinationPrivate GnomeBackupDestinationPrivate;

struct {
	GObject parent;
} GnomeBackupDestination;

struct {
	GObjectClass parent_class;
} GnomeBackupDestinationClass;

G_END_DECLS

#endif
