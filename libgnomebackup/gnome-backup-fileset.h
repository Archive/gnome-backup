/* GNOME Backup library
 * Copyright (C) 2003 GNOME Foundation
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gnome_backup_fileset_h__)
#  define __gnome_backup_fileset_h__

#include <glib-object.h>

G_BEGIN_DECLS

#define GNOME_BACKUP_TYPE_FILESET            (gnome_backup_fileset_get_type())
#define GNOME_BACKUP_FILESET(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_BACKUP_TYPE_FILESET, GnomeBackupFileset))
#define GNOME_BACKUP_FILESET_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_BACKUP_TYPE_FILESET, GnomeBackupFilesetClass))
#define GNOME_BACKUP_IS_FILESET(obj)         (G_TYPE_CHECK_INSTANCE_TYPE(obj, GNOME_BACKUP_TYPE_FILESET))
#define GNOME_BACKUP_IS_FILESET_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GNOME_BACKUP_TYPE_FILESET))

typedef struct _GnomeBackupFilesetPrivate GnomeBackupFilesetPrivate;

typedef struct {
	GObject parent;
	GnomeBackupFilesetPrivate *priv;
} GnomeBackupFileset;

typedef struct {
	GObjectClass parent_class;
} GnomeBackupFilesetClass;

GType                     gnome_backup_fileset_get_type (void);
GnomeBackupFileset       *gnome_backup_fileset_new (void);

const gchar              *gnome_backup_fileset_get_name (GnomeBackupFileset *fs);
void                      gnome_backup_fileset_set_name (GnomeBackupFileset *fs, const gchar *name);
const GDate              *gnome_backup_fileset_get_last_backup (GnomeBackupFileset *fs);
void                      gnome_backup_fileset_set_last_backup (GnomeBackupFileset *fs, const GDate *last_backup);

void                      gnome_backup_fileset_add_path (GnomeBackupFileset *fs, const gchar *path);

const GnomeBackupFileset *gnome_backup_fileset_get_by_name (const gchar *name);
GSList                   *gnome_backup_fileset_get_list (void);

G_END_DECLS

#endif
